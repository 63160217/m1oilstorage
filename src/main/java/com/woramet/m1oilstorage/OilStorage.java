/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.woramet.m1oilstorage;

/**
 *
 * @author User
 */
public class OilStorage {
    String nameOil;
    double amountOil;
    double money;
    double buy;
    double sell;
    OilStorage (String nameOil ,double amountOil ,double money){
        this.nameOil = nameOil;
        this.amountOil = amountOil;
        this.money = money;
    }
    
    void increase (double amountOil ,double price){
        buy = amountOil*price;
        if (amountOil <= 0){
            System.out.println("Amount Oil need more 0 !!!");
            System.out.println("------------------------------");
            return;
        }
        if (money < buy ){
            System.out.println("Not enough money !!!");
            System.out.println("------------------------------");
            return;
        }
        this.amountOil = this.amountOil + amountOil;
        this.money = this.money - buy;
        System.out.println("Name Oil : " + nameOil);
        System.out.println("Remaining Oil : " + this.amountOil);
        System.out.println("Balance : " + money);
        System.out.println("------------------------------");
    }
    
     void decrease (double amountOil ,double price){
         sell = amountOil*price;
         if (amountOil <= 0){
             System.out.println("Amount Oil need more 0 !!!");
             System.out.println("------------------------------");
             return;
         }
         if (amountOil > this.amountOil){
             System.out.println("Not enough Amount Oil");
             System.out.println("------------------------------");
             return;
         }
         this.amountOil = this.amountOil - amountOil;
         this.money = this.money + sell;
        System.out.println("Name Oil : " + nameOil);
        System.out.println("Remaining Oil : " + this.amountOil);
        System.out.println("Balance : " + money);
        System.out.println("------------------------------");
     }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.woramet.m1oilstorage;

/**
 *
 * @author User
 */
public class TestOilStorage {
    public static void main(String[] args){
        OilStorage Oil1 = new OilStorage ("G95" ,200 ,10000);
        OilStorage Oil2 = new OilStorage ("E20" ,50 ,600);
        OilStorage Oil3 = new OilStorage ("E85" ,100 ,5000);
        Oil1.increase(100, 30);
        Oil2.increase(100, 20);
        Oil2.increase(0, 20);
        Oil2.increase(200, 0);
        Oil3.decrease(100, 40);
        Oil3.decrease(50, 20);
        Oil2.decrease(0, 0);
    }
}
